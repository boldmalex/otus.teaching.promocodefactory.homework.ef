﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain
{
    public static class DomainConstants
    {
        /// <summary>
        /// Длительность действия промокода в днях
        /// </summary>
        public const int constDefaultPromoCodePeriodDays = 10;
    }
}
