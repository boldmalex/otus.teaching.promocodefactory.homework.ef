﻿using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer : BaseEntity
    {
        public Customer()
        {
            CustomerPreferences = new List<CustomerPreference>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public virtual List<CustomerPreference> CustomerPreferences { get; set; }

        public virtual PromoCode PromoCode { get; set; }

    }
}