﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Api.Dto;
using Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения сотрудников
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : Controller
    {

        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }


        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns>Предпочтения</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<List<PreferenceResponseDto>>> GetEmployeesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();

            var preferencesDto = preferences.Select(p=> p.ToDtoModel());

            return Ok(preferencesDto);
        }
    }
}
