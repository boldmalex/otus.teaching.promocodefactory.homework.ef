﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Api.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teching.PromoCodeFactory.Services.Abstractions;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {

        private readonly IPromoCodeDtoService _promoCodeDtoService;

        public PromocodesController(IPromoCodeDtoService promoCodeDtoService)
        {
            _promoCodeDtoService = promoCodeDtoService;
        }



        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns>Промокоды</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponseDto>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeDtoService.GetAllDtoModelsAsync();
            return Ok(promoCodes);
        }



        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequestDto request)
        {
            var isExistPreference = await _promoCodeDtoService.PromoCodePreferenceExistsAsync(request.Preference);

            if (!isExistPreference)
            {
                return NotFound(request.Preference);
            }

            await _promoCodeDtoService.GivePromoCodeAsync(request);

            return Ok();
        }
    }
}