﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Api.Dto;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {

        private readonly ICustomerDtoService _customerService;

        public CustomersController(ICustomerDtoService customerService)
        {
            _customerService = customerService;
        }


        /// <summary>
        /// Получить данные клиентов
        /// </summary>
        /// <returns>Клиенты</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerService.GetAllDtoShortModelsAsync();

            return Ok(customers.ToList());
        }


        /// <summary>
        /// Получить данные клиента по Id
        /// </summary>
        /// <returns>Клиент</returns>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpGet("{id:guid}", Name="GetCustomer")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CustomerResponseDto>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerService.GetDtoModelByIdAsync(id);

            if (customer == null)
                return NotFound();

            return Ok(customer);
        }



        /// <summary>
        /// Добавить клиента
        /// </summary>
        /// <param name="request">Данные нового клиента</param>
        /// <returns>Новый клиент</returns>
        /// <response code="201"> Возвращает нового клиента</response>
        /// <response code="400"> Ошибка</response>  
        [HttpPost]
        [ProducesResponseType(typeof(EmployeeResponse), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<CustomerResponseDto>> CreateCustomerAsync(CreateOrEditCustomerRequestDto request)
        {
            var newCustomer = await _customerService.AddAsync(request);

            return CreatedAtRoute("GetCustomer", new { id = newCustomer.CustomerId }, newCustomer);
        }



        /// <summary>
        /// Изменить клиента
        /// </summary>
        /// <param name="id">ИД клиента</param>
        /// <param name="request">Новые данные клиента</param>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequestDto request)
        {
            // Проверим наличие клиента
            var isExist = await _customerService.CustomerExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Обновляем
            await _customerService.UpdateAsync(id, request);

            return Ok();
        }



        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <response code="200"> Ok</response>
        /// <response code="400"> Ошибка</response>
        /// <response code="404"> Не найдено</response>
        [HttpDelete("{id:guid}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteCustomer(Guid id)
        {
            // Проверим наличие клиента
            var isExist = await _customerService.CustomerExistsAsync(id);

            if (!isExist)
            {
                return NotFound();
            }

            // Удаляем
            await _customerService.DeleteAsync(id);

            return Ok();
        }
    }
}