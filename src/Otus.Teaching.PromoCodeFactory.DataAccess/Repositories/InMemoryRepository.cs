﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T> : IRepository<T> where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public IQueryable<T> Table => throw new NotImplementedException();

        public InMemoryRepository(IList<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult((IEnumerable<T>)Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> GetByIdsAsync(IEnumerable<Guid> ids)
        {
            return Task.FromResult(Data.Where(x => ids.Contains(x.Id)));
        }

        public Task InsertAsync(T entity)
        {
            entity.Id = Guid.NewGuid();
            Data.Add(entity);

            return Task.CompletedTask;
        }

        public async Task UpdateAsync(T entity)
        {
            var existingEntity = await GetByIdAsync(entity.Id);

            if (existingEntity != null)
            {
                var entityIndex = Data.IndexOf(existingEntity);
                Data[entityIndex] = entity;
            }
        }

        public async Task DeleteAsync(T entity)
        {
            var existingEntity = await GetByIdAsync(entity.Id);

            if (existingEntity != null)
            {
                Data.Remove(existingEntity);
            }
        }
    }
}