﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DataContext : DbContext
    {
        private const int constStringFieldMaxLength = 512;
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        { }


        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(c => c.Id);
            modelBuilder.Entity<Customer>().Property(p => p.FirstName).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Customer>().Property(p => p.LastName).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Customer>().Property(p => p.Email).HasMaxLength(constStringFieldMaxLength);

            modelBuilder.Entity<CustomerPreference>().HasKey(cp => cp.Id);
            modelBuilder.Entity<CustomerPreference>().HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
            modelBuilder.Entity<CustomerPreference>().Property(p => p.Id).ValueGeneratedOnAdd().HasValueGenerator<GuidValueGenerator>().IsRequired();

            modelBuilder.Entity<Preference>().HasKey(p => p.Id);
            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(constStringFieldMaxLength);

            modelBuilder.Entity<PromoCode>().HasKey(p => p.Id);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<PromoCode>().Property(p => p.Code).HasMaxLength(constStringFieldMaxLength);

            modelBuilder.Entity<Employee>().HasKey(p => p.Id);
            modelBuilder.Entity<Employee>().Property(p => p.FirstName).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Employee>().Property(p => p.LastName).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Employee>().Property(p => p.Email).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Employee>().Property(p => p.AppliedPromocodesCount).HasDefaultValue(0);

            modelBuilder.Entity<Role>().HasKey(p => p.Id);
            modelBuilder.Entity<Role>().Property(p => p.Name).HasMaxLength(constStringFieldMaxLength);
            modelBuilder.Entity<Role>().Property(p => p.Description).HasMaxLength(constStringFieldMaxLength);
        }
    }
}
