﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DbInizializer
{
    public interface IDbInizializer
    {
        public void InitializeDb();
    }
}
