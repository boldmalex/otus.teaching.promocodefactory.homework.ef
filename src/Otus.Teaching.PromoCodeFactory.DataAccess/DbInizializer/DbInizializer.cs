﻿using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DbInizializer
{
    public class DbInizializer : IDbInizializer
    {
        private readonly DataContext _dataContext;

        public DbInizializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();

            _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChanges();

            _dataContext.Customers.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();
        }
    }
}
