﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto
{
    public class PromoCodeShortResponseDto
    {
        public Guid Id { get; set; }
        public string Code { get; set; }
        public string ServiceInfo { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
        public string PartnerName { get; set; }
    }
}
