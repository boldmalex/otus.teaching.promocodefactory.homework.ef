﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto
{
    public class CustomerResponseDto
    {
        public CustomerResponseDto()
        {
            Preferences = new List<PreferenceResponseDto>();
        }

        public Guid CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public List<PreferenceResponseDto> Preferences { get; set; }
        public List<PromoCodeShortResponseDto> PromoCodes { get; set; }
    }
}
