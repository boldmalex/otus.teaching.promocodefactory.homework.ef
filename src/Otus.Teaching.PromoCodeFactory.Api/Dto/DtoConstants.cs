﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto
{
    public static class DtoConstants
    {
        /// <summary>
        /// Формат дат для вывода в DTO
        /// </summary>
        public const string constDtoDateFormat = "dd.MM.yyyy HH:mm";
       
    }
}
