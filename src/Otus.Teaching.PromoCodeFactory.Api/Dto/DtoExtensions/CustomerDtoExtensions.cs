﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions
{
    public static class CustomerDtoExtensions
    {
        public static CustomerResponseDto ToDtoModel(this Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            CustomerResponseDto result = new CustomerResponseDto()
            {
                CustomerId = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = customer.CustomerPreferences.Select(p => p.Preference.ToDtoModel()).ToList(),
                PromoCodes = new List<PromoCodeShortResponseDto>() 
            };

            if (customer.PromoCode != null)
                result.PromoCodes.Add(customer.PromoCode.ToDtoModel());

            return result;
        }

        public static CustomerShortResponseDto ToShortDtoModel(this Customer customer)
        {
            if (customer == null)
                throw new ArgumentNullException(nameof(customer));

            CustomerShortResponseDto result = new CustomerShortResponseDto()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
            };

            return result;
        }
    }
}
