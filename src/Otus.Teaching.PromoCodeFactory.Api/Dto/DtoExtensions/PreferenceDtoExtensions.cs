﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions
{
    public static class PreferenceDtoExtensions
    {
        public static PreferenceResponseDto ToDtoModel(this Preference preference)
        {
            if (preference == null)
                throw new ArgumentNullException(nameof(preference));

            PreferenceResponseDto result = new PreferenceResponseDto()
            {
                Id = preference.Id,
                Name = preference.Name
            };

            return result;
        }
    }
}
