﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions
{
    public static class PromoCodeDtoExtensions
    {
        public static PromoCodeShortResponseDto ToDtoModel(this PromoCode promoCode)
        {
            if (promoCode == null)
                throw new ArgumentNullException(nameof(promoCode));

            PromoCodeShortResponseDto result = new PromoCodeShortResponseDto()
            {
                Id = promoCode.Id,
                Code = promoCode.Code,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo,
                BeginDate = promoCode.BeginDate.ToString(DtoConstants.constDtoDateFormat),
                EndDate = promoCode.EndDate.ToString(DtoConstants.constDtoDateFormat)
            };

            return result;
        }
    }
}
