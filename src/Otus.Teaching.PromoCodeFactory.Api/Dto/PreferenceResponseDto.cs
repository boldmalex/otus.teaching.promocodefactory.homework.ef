﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.Api.Dto
{
    
    public class PreferenceResponseDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
