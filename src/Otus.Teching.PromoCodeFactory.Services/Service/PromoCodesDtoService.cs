﻿using Otus.Teaching.PromoCodeFactory.Api.Dto;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teching.PromoCodeFactory.Services.Abstractions;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teching.PromoCodeFactory.Services.Service
{
    public class PromoCodesDtoService : IPromoCodeDtoService
    {
        
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromoCodesDtoService(IRepository<PromoCode> promoCodeRepository,
                                    IRepository<Preference> preferenceRepository,
                                    IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }


        public async Task<List<PromoCodeShortResponseDto>> GetAllDtoModelsAsync()
        {
            var existingPromoCodes = await _promoCodeRepository.GetAllAsync();

            return existingPromoCodes.Select(pm => pm.ToDtoModel()).ToList();
        }


        public async Task GivePromoCodeAsync(GivePromoCodeRequestDto givePromoCodeRequestDto)
        {
            if (givePromoCodeRequestDto == null)
                throw new ArgumentNullException(nameof(givePromoCodeRequestDto));

            // Проверяем наличие предпочтения
            Preference preference = _preferenceRepository.Table.Where(p=>p.Name == givePromoCodeRequestDto.Preference).FirstOrDefault();

            if (preference == null)
                throw new ArgumentNullException(nameof(preference));

            var promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                Code = givePromoCodeRequestDto.PromoCode,
                ServiceInfo = givePromoCodeRequestDto.ServiceInfo,
                PartnerName = givePromoCodeRequestDto.PartnerName,
                BeginDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(DomainConstants.constDefaultPromoCodePeriodDays),
                Preference = preference
            };

            // Добавляем промокод
            await _promoCodeRepository.InsertAsync(promoCode);

            // Добавляем промокод клиентам c указанным предпочтением
            var customers = _customerRepository.Table.Where(c => c.CustomerPreferences.Any(cp => cp.PreferenceId == preference.Id));

            foreach (var customer in customers)
            {
                customer.PromoCode = promoCode;
                await _customerRepository.UpdateAsync(customer);
            }
        }

        public async Task<bool> PromoCodePreferenceExistsAsync(string name)
        {
            var preference = _preferenceRepository.Table.Where(p => p.Name == name).FirstOrDefault();

            return preference != null;
        }
    }
}
