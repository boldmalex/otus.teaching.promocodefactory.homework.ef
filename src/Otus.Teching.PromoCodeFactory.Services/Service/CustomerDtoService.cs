﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Api.Dto;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Services.Abstractions.Services;
using Otus.Teaching.PromoCodeFactory.Api.Dto.DtoExtensions;

namespace Otus.Teaching.PromoCodeFactory.Services.Service
{
    public class CustomerDtoService : ICustomerDtoService
    {

        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomerDtoService(IRepository<Customer> customerRepository,
                                  IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }


        public async Task<bool> CustomerExistsAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            return customer != null;
        }



        public async Task<IEnumerable<CustomerResponseDto>> GetAllDtoModelsAsync()
        {

            var customers = await _customerRepository.GetAllAsync();

            var result = customers.Select(x => x.ToDtoModel());

            return result;
        }



        public async Task<IEnumerable<CustomerShortResponseDto>> GetAllDtoShortModelsAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var result = customers.Select(x => x.ToShortDtoModel());

            return result;
        }



        public async Task<CustomerResponseDto> GetDtoModelByIdAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            return customer.ToDtoModel();
        }



        public async Task<CustomerResponseDto> AddAsync(CreateOrEditCustomerRequestDto createOrEditCustomerRequestDto)
        {
            if (createOrEditCustomerRequestDto == null)
                throw new ArgumentNullException(nameof(createOrEditCustomerRequestDto));


            List<Preference> newCustomerPreferences = _preferenceRepository.Table.Where(p => createOrEditCustomerRequestDto.PreferenceIds.Contains(p.Id)).ToList();

            Guid newCustomerGuid = Guid.NewGuid();

            // Создаем клиента для сохранения
            Customer customer = new Customer()
            {
                Id = newCustomerGuid,
                Email = createOrEditCustomerRequestDto.Email,
                FirstName = createOrEditCustomerRequestDto.FirstName,
                LastName = createOrEditCustomerRequestDto.LastName,
            };

            if (newCustomerPreferences.Any())
            {
                foreach (var preference in newCustomerPreferences)
                {
                    customer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        CustomerId = newCustomerGuid,
                        PreferenceId = preference.Id
                    });
                }
            }

            // Сохраняем
            await _customerRepository.InsertAsync(customer);

            // Возвращаем
            var createdCustomer = await _customerRepository.GetByIdAsync(newCustomerGuid);
            return createdCustomer.ToDtoModel();
        }


        
        public async Task UpdateAsync(Guid id, CreateOrEditCustomerRequestDto createOrEditCustomerRequestDto)
        {
            if (createOrEditCustomerRequestDto == null)
                throw new ArgumentNullException(nameof(createOrEditCustomerRequestDto));

            var existingCustomer = await _customerRepository.GetByIdAsync(id);

            List<Preference> newCustomerPreferences = _preferenceRepository.Table.Where(p => createOrEditCustomerRequestDto.PreferenceIds.Contains(p.Id)).ToList();

            // Обновляем
            existingCustomer.FirstName = createOrEditCustomerRequestDto.FirstName;
            existingCustomer.LastName = createOrEditCustomerRequestDto.LastName;
            existingCustomer.Email = createOrEditCustomerRequestDto.Email;
            existingCustomer.CustomerPreferences.Clear();

            if (newCustomerPreferences.Any())
            {
                foreach (var preference in newCustomerPreferences)
                {
                    existingCustomer.CustomerPreferences.Add(new CustomerPreference()
                    {
                        CustomerId = existingCustomer.Id,
                        PreferenceId = preference.Id
                    });
                }
            }

            // Сохраняем
            await _customerRepository.UpdateAsync(existingCustomer);
        }



        public async Task DeleteAsync(Guid id)
        {
            var existingCustomer = await _customerRepository.GetByIdAsync(id);

            await _customerRepository.DeleteAsync(existingCustomer);
        }
    }
}
