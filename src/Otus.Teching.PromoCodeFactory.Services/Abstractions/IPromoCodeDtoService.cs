﻿using Otus.Teaching.PromoCodeFactory.Api.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teching.PromoCodeFactory.Services.Abstractions
{
    public interface IPromoCodeDtoService
    {
        Task<bool> PromoCodePreferenceExistsAsync(string name);
        Task<List<PromoCodeShortResponseDto>> GetAllDtoModelsAsync();
        Task GivePromoCodeAsync(GivePromoCodeRequestDto givePromoCodeRequestDto);

    }
}
