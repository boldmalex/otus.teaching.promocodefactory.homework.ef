﻿using Otus.Teaching.PromoCodeFactory.Api.Dto;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Services.Abstractions.Services
{
    public interface ICustomerDtoService
    {
        Task<bool> CustomerExistsAsync(Guid id);
        Task<IEnumerable<CustomerResponseDto>> GetAllDtoModelsAsync();
        Task<IEnumerable<CustomerShortResponseDto>> GetAllDtoShortModelsAsync();
        Task<CustomerResponseDto> GetDtoModelByIdAsync(Guid id);
        Task<CustomerResponseDto> AddAsync(CreateOrEditCustomerRequestDto createOrEditCustomerRequestDto);
        Task UpdateAsync(Guid id, CreateOrEditCustomerRequestDto createOrEditCustomerRequestDto);
        Task DeleteAsync(Guid id);
    }
}
